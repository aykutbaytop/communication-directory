﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PeBook.Services.PersonContact.Business.Abstract;
using PeBook.Services.PersonContact.Entities.DTOs;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PhoneBook.Services.Person.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PersonContactInfoController : ControllerBase
    {
        private IPersonContactInfoService _personContactInfoService;

        public PersonContactInfoController(IPersonContactInfoService personContactInfoService)
        {
            _personContactInfoService = personContactInfoService;
        }

        [HttpPost("addPersonContactInfo")]
        public async Task<IActionResult> AddPersonContactInfo(PersonContactInfoDto model)
        {
            await _personContactInfoService.AddAsync(model);

            return Ok();
        }

        [HttpDelete("deletePersonContact")]
        public async Task<IActionResult> DeletePersonContact(Guid id)
        {
            await _personContactInfoService.DeleteAsync(id);

            return Ok();
        }

        [HttpGet("getContactListByPersonId")]
        public async Task<IActionResult> GetContactListByPersonId(Guid id)
        {
            var result = await _personContactInfoService.GetContactListByPersonId(id);

            return Ok(result);
        }

        [HttpGet("getReportData")]
        public async Task<IActionResult> GetReportData()
        {
            var result = await _personContactInfoService.GetReportData();

            return Ok(result);
        }
    }
}

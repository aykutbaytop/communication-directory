﻿using Microsoft.AspNetCore.Mvc;
using PeBook.Services.PersonContact.Business.Abstract;
using PeBook.Services.PersonContact.Entities.Concrete;
using PeBook.Services.PersonContact.Entities.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PeBook.Services.PersonContact.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PersonController : ControllerBase
    {
        IPersonService _personService;
        public PersonController(IPersonService personService)
        {
            _personService = personService;
        }

        [HttpPost("add")]
        public async Task<IActionResult> Add(PersonDto person)
        {
            await _personService.AddAsync(person);

            return Ok();
        }

        [HttpPost("update")]
        public async Task<IActionResult> Update(PersonDto person)
        {
            await _personService.UpdateAsync(person);

            return Ok();
        }

        [HttpDelete("delete")]
        public async Task<IActionResult> Delete(Guid id)
        {
            await _personService.Remove(id);

            return Ok();
        }

        [HttpGet("getall")]
        public async Task<IActionResult> GetAll()
        {
            var result = await _personService.GetListAsync();

            return Ok(result);
        }

        [HttpGet("getPersonWithContactsByPersonId")]
        public async Task<IActionResult> GetPersonWithContactsByPersonId(Guid id)
        {
            var result = await _personService.GetPersonWithContactsByPersonId(id);

            return Ok(result);
        }

    }
}

﻿using DataAccess.Abstract;
using Microsoft.EntityFrameworkCore;
using PeBook.Services.PersonContact.Core.DataAccess.EntityFramework;
using PeBook.Services.PersonContact.Entities.Concrete;
using PeBook.Services.PersonContact.Entities.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PeBook.Services.PersonContact.DataAccess.Concrete
{
    public class EfPersonContactInfoDal : EfEntityRepository<PersonContactInfo, PersonContactDbContext>, IPersonContactInfoDal
    {
        public async Task<IEnumerable<LocationReportDto>> GetReportData()
        {

            using (PersonContactDbContext context = new PersonContactDbContext())
            {
                return await (from person in context.Persons
                                      join pc in context.PersonContacts 
                                        on person.Id equals pc.PersonId
                                      where pc.ContactTypeId == 3
                                      group pc by pc.Info into list
                                      select new LocationReportDto
                                      {
                                          Location = list.Key,
                                          PersonCount = list.Count(),
                                          PhoneCount = (from subPc in context.PersonContacts.Where(pc => pc.ContactTypeId == 1)
                                                                    join subP in context.Persons on subPc.PersonId equals subP.Id
                                                                    where (from subPc2 in context.PersonContacts where subPc2.PersonId == subP.Id && subPc2.Info == list.Key select subPc2).Any()
                                                                    select subPc).Count(),
                                      }).ToListAsync();

            }
        }
    }
}

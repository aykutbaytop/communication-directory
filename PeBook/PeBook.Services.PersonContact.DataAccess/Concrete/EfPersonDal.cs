﻿using DataAccess.Abstract;
using Microsoft.EntityFrameworkCore;
using PeBook.Services.PersonContact.Core.DataAccess.EntityFramework;
using PeBook.Services.PersonContact.Entities.Concrete;
using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PeBook.Services.PersonContact.DataAccess.Concrete
{
    public class EfPersonDal : EfEntityRepository<Person, PersonContactDbContext>, IPersonDal
    {
        public async Task<Person> GetPersonWithContactsByPersonId(Guid id)
        {
            using (PersonContactDbContext context = new PersonContactDbContext())
            {
                return await context.Persons.Where(p => p.Id.Equals(id)).Include(p => p.ContactInfos).ThenInclude(ci => ci.ContactType) .FirstOrDefaultAsync();
            }
        }
    }
}

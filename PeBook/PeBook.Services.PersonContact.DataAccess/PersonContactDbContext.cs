﻿using Microsoft.EntityFrameworkCore;
using PeBook.Services.PersonContact.DataAccess.Configration;
using PeBook.Services.PersonContact.Entities.Concrete;

namespace PeBook.Services.PersonContact.DataAccess
{
    public class PersonContactDbContext : DbContext
    {
        public PersonContactDbContext()
        {

        }
        public PersonContactDbContext(DbContextOptions<PersonContactDbContext> options) : base(options){}

        public DbSet<Person> Persons { get; set; }

        public DbSet<PersonContactInfo> PersonContacts { get; set; }

        public DbSet<ContactType> ContactTypes { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new PersonConfiguration());

            modelBuilder.ApplyConfiguration(new PersonContactInfoConfiguration());

            modelBuilder.ApplyConfiguration(new ContactTypeConfiguration());
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseNpgsql(@"UserName=postgres;Password=aykut123.;Server=localhost;Port=5432;Database=PeBook;Integrated Security=true;Pooling=true;"
                    , o => { o.MigrationsAssembly("PeBook.Services.PersonContact.DataAccess"); }
                   );
        }
    }
}

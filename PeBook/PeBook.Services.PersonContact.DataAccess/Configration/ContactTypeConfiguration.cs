﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PeBook.Services.PersonContact.Entities.Concrete;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PeBook.Services.PersonContact.DataAccess.Configration
{
    public class ContactTypeConfiguration : IEntityTypeConfiguration<ContactType>
    {
        public void Configure(EntityTypeBuilder<ContactType> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).ValueGeneratedOnAdd();

            builder.Property(x => x.Definition).IsRequired();

            builder
                .HasData(new ContactType
                {
                    Id = 1,
                    Definition = "Phone"
                },
                new ContactType
                {
                    Id = 2,
                    Definition = "Email"
                },
                new ContactType
                {
                    Id = 3,
                    Definition = "Location"
                }
            );
        }
    }
}
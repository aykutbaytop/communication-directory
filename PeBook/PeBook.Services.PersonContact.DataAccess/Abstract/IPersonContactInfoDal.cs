﻿using PeBook.Services.PersonContact.Core.DataAccess;
using PeBook.Services.PersonContact.Entities.Concrete;
using PeBook.Services.PersonContact.Entities.DTOs;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Abstract
{
    public interface IPersonContactInfoDal:IEntityRepository<PersonContactInfo>
    {
        Task<IEnumerable<LocationReportDto>> GetReportData();
    }
}

﻿using PeBook.Services.PersonContact.Core.DataAccess;
using PeBook.Services.PersonContact.Entities.Concrete;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Abstract
{
    public interface IPersonDal:IEntityRepository<Person>
    {
        Task<Person> GetPersonWithContactsByPersonId(Guid id);
    }
}

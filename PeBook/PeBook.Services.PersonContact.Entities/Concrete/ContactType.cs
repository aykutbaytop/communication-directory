﻿using PeBook.Services.PersonContact.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PeBook.Services.PersonContact.Entities.Concrete
{
    public class ContactType : IEntity
    {
        public int Id { get; set; }
        public string Definition { get; set; }
    }
}

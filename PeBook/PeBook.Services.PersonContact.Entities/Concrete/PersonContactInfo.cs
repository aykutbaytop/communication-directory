﻿using PeBook.Services.PersonContact.Core.Entities;
using System;

namespace PeBook.Services.PersonContact.Entities.Concrete
{
    public class PersonContactInfo : IEntity
    {
        public Guid Id { get; set; }

        public Guid PersonId { get; set; }
        public virtual Person Person { get; set; }

        public int ContactTypeId { get; set; }
        public virtual ContactType ContactType { get; set; }

        public string Info { get; set; }
    }
}

﻿using AutoMapper;
using DataAccess.Abstract;
using PeBook.Services.PersonContact.Business.Abstract;
using PeBook.Services.PersonContact.Entities.Concrete;
using PeBook.Services.PersonContact.Entities.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PeBook.Services.PersonContact.Business.Concrete
{
    public class PersonContactInfoManager : IPersonContactInfoService
    {
        private IPersonContactInfoDal _personContactInfoDal;
        private IMapper _mapper;

        public PersonContactInfoManager(IPersonContactInfoDal personContactInfoDal, IMapper mapper)
        {
            _personContactInfoDal = personContactInfoDal;
            _mapper = mapper;
        }

        public async Task AddAsync(PersonContactInfoDto contact)
        {
            await _personContactInfoDal.AddAsync(_mapper.Map<PersonContactInfo>(contact));
        }

        public async Task DeleteAsync(Guid id)
        {
            PersonContactInfo personContactInfo =  _personContactInfoDal.Where(ci => ci.Id == id).GetAwaiter().GetResult().FirstOrDefault();
            
            await _personContactInfoDal.Remove(personContactInfo);
        }

        public async Task<IEnumerable<PersonContactInfoDto>> GetContactListByPersonId(Guid id)
        {
            IEnumerable<PersonContactInfo> results = await _personContactInfoDal.Where(pci => pci.PersonId.Equals(id));
                
            return _mapper.Map<List<PersonContactInfoDto>>(results);
        }

        public async Task<IEnumerable<LocationReportDto>> GetReportData()
        {
            return await _personContactInfoDal.GetReportData();
        }
    }
}

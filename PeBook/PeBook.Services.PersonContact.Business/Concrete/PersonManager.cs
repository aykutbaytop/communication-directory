﻿using AutoMapper;
using DataAccess.Abstract;
using PeBook.Services.PersonContact.Business.Abstract;
using PeBook.Services.PersonContact.Entities.Concrete;
using PeBook.Services.PersonContact.Entities.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PeBook.Services.PersonContact.Business.Concrete
{
    public class PersonManager : IPersonService
    {
        private IPersonDal _personDal;
        private IMapper _mapper;

        public PersonManager(IPersonDal personDal, IMapper mapper)
        {
            _personDal = personDal;
            _mapper = mapper;
        }

        public async Task AddAsync(PersonDto personDto)
        {
            await _personDal.AddAsync(_mapper.Map<Person>(personDto));
        }

        public async Task UpdateAsync(PersonDto personDto)
        {
            await _personDal.Update(_mapper.Map<Person>(personDto));
        }
        
        public async Task<IEnumerable<Person>> GetListAsync()
        {
            return await _personDal.GetAllAsync();
        }

        public async Task Remove(Guid id)
        {
            var entity = _personDal.Where(p => p.Id.Equals(id)).GetAwaiter().GetResult().FirstOrDefault();

            await _personDal.Remove(entity);
        }

        public async Task<Person> GetPersonWithContactsByPersonId(Guid id)
        {
            return await _personDal.GetPersonWithContactsByPersonId(id);
        }
    }
}

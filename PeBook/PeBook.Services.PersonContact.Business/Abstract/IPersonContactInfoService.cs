﻿using PeBook.Services.PersonContact.Entities.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PeBook.Services.PersonContact.Business.Abstract
{
    public interface IPersonContactInfoService
    {
        Task AddAsync(PersonContactInfoDto contact);
        Task DeleteAsync(Guid id);
        Task<IEnumerable<PersonContactInfoDto>> GetContactListByPersonId(Guid id);
        Task<IEnumerable<LocationReportDto>> GetReportData();
    }
}

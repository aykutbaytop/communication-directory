﻿using DataAccess.Abstract;
using PeBook.Services.PersonContact.Entities.Concrete;
using PeBook.Services.PersonContact.Entities.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PeBook.Services.PersonContact.Business.Abstract
{
    public interface IPersonService
    {
        Task AddAsync(PersonDto personDto);
        Task UpdateAsync(PersonDto personDto);
        Task Remove(Guid id);
        Task<IEnumerable<Person>> GetListAsync();
        Task<Person> GetPersonWithContactsByPersonId(Guid id);
    }
}

﻿using Autofac;
using AutoMapper;
using DataAccess.Abstract;
using PeBook.Services.PersonContact.Business.Abstract;
using PeBook.Services.PersonContact.Business.AutoMapper;
using PeBook.Services.PersonContact.Business.Concrete;
using PeBook.Services.PersonContact.DataAccess.Concrete;
using PeBook.Services.PersonContact.Entities.Concrete;
using PeBook.Services.PersonContact.Entities.DTOs;
using System;
using System.Collections.Generic;
using System.Text;

namespace Business.DependencyResolvers.Autofac
{
    public class AutofacBusinessModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<PersonManager>().As<IPersonService>().SingleInstance();
            builder.RegisterType<EfPersonDal>().As<IPersonDal>().SingleInstance();

            builder.RegisterType<PersonContactInfoManager>().As<IPersonContactInfoService>().SingleInstance();
            builder.RegisterType<EfPersonContactInfoDal>().As<IPersonContactInfoDal>().SingleInstance();

            builder.RegisterType<MapProfile>().As<Profile>();
            builder.Register(c => new MapperConfiguration(cfg =>
            {
                foreach (var profile in c.Resolve<IEnumerable<Profile>>())
                {
                    cfg.AddProfile(profile);
                }
            })).AsSelf().SingleInstance();

            builder.Register(c => c.Resolve<MapperConfiguration>().CreateMapper(c.Resolve)).As<IMapper>().InstancePerLifetimeScope();
        }
    }
}
